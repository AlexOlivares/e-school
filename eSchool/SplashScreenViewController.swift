//
//  SplashScreenViewController.swift
//  eSchool
//
//  Created by Juan Carlos on 30/09/17.
//  Copyright © 2017 Juan Carlos. All rights reserved.
//

import UIKit
import Foundation

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        perform(Selector("showNavController"), with: nil, afterDelay: 2)
        //Add segue identificator for the if...
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showNavController() {
        performSegue(withIdentifier: "termsAndConditions", sender: self)
    }
}
