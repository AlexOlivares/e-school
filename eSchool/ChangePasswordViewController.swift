//
//  ChangePasswordViewController.swift
//  eSchool
//
//  Created by eBitware on 03/10/17.
//  Copyright © 2017 Juan Carlos. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var verifiedPasswordTextField: UITextField!
    
    var userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Cambiar Contraseña"
        
        newPasswordTextField.isSecureTextEntry = true
        verifiedPasswordTextField.isSecureTextEntry = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func verifyTextFields() {
        
        let alertController = UIAlertController(title: "Passwords No Iguales", message: "Los datos ingresados deben ser iguales", preferredStyle: UIAlertControllerStyle.alert )
        alertController.addAction(UIAlertAction(title: "Reintentar", style: UIAlertActionStyle.default, handler: nil))
       
        if(newPasswordTextField.text != verifiedPasswordTextField.text) {
             self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func unwindFunction(_ sender: UIButton) {
    
        if(newPasswordTextField.text == verifiedPasswordTextField.text && newPasswordTextField.text != nil) {
            userDefaults.set((newPasswordTextField.text as! String), forKey: "Password")
            performSegue(withIdentifier: "unwindSegue", sender: self)
        } else {
            verifyTextFields()
        }
    }
}
