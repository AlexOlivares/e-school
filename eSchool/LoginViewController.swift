//
//  LoginViewController.swift
//  eSchool
//
//  Created by eBitware on 02/10/17.
//  Copyright © 2017 Juan Carlos. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var escuelaTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usuarioTextField: UITextField!
    
    let userDefaults = UserDefaults.standard
    let pickerOptionEscuela = [ "Colegio Altamira", "Colegio Insurgentes", "Liceo del Valle", "Centro Educativo Camino Real", "Liceo Consuelo", "Instituto La Salle", "Colegio Internacional", "Colegio Iberoamericano", "Colegio La Paz", "Escuela Superior de Veracruz" ]
    let estadoPickerView = UIPickerView()
    var messageAlert: String = ""
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpDetails()
        estadoPickerView.delegate = self
        escuelaTextField.inputView = estadoPickerView
        passwordTextField.isSecureTextEntry = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setUpDetails() {
        
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
       
        escuelaTextField.textAlignment = .center
        passwordTextField.textAlignment = .center
        usuarioTextField.textAlignment = .center
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerOptionEscuela.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerOptionEscuela[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        escuelaTextField.text = (pickerOptionEscuela)[row]
    }
    @IBAction func LoginButtonPressed(_ sender: Any) {
        
        let correctUser = userDefaults.value(forKey: "Usuario") as! String
        let correctPassword = userDefaults.value(forKey: "Password") as! String
        let correctSchool = userDefaults.value(forKey: "EscuelaUsuario") as! String
        
        if(escuelaTextField.text == correctSchool && (usuarioTextField.text == correctUser) && (passwordTextField.text == correctPassword)) {
            performSegue(withIdentifier: "changePasswordSegue", sender: self)
        } else {
            if(escuelaTextField.text == "") {
                messageAlert = "Ingrese una escuela valida"
            } else if(usuarioTextField.text == "" || usuarioTextField.text != (userDefaults.value(forKey: "Usuario") as! String)) {
                messageAlert = "Ingrese un usuario valido"
            } else if(passwordTextField.text == "" || passwordTextField.text != userDefaults.value(forKey: "Password") as! String) {
                messageAlert = "Ingrese una contraseña valida"
            }
            let alertController = UIAlertController(title: "Datos Incorrectos", message: "\(messageAlert)", preferredStyle: UIAlertControllerStyle.alert )
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default ,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func myUnwindSegue(unwindSegue: UIStoryboardSegue) {
        
    }
}
