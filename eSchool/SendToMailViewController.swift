//
//  SendToMailViewController.swift
//  eSchool
//
//  Created by eBitware on 04/10/17.
//  Copyright © 2017 Juan Carlos. All rights reserved.
//

import UIKit
import MessageUI

class SendToMailViewController: UIViewController, MFMailComposeViewControllerDelegate{

    
    @IBOutlet weak var mailTextField: UITextField!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendToMailPressed(_ sender: Any) {
        
        let userDefaults = UserDefaults.standard
        let correoUsuario = userDefaults.value(forKey: "Correo")
        let alertController = UIAlertController(title: "Correo invalido", message: "Este correo no se encuentra asociado a alguna cuenta", preferredStyle: UIAlertControllerStyle.alert
        )
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
    
        if( mailTextField.text == "" || mailTextField.text !=  correoUsuario as? String) {
            self.present(alertController, animated: true, completion: nil)
        } else {
            
            performSegue(withIdentifier: "myUnwindSegue", sender: self)
        }
    }
}
