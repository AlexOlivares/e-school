//
//  TermsAndConditionsViewController.swift
//  eSchool
//
//  Created by Juan Carlos on 30/09/17.
//  Copyright © 2017 Juan Carlos. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
       
        UserDefaults.standard.set("A012345", forKey: "Usuario")
        UserDefaults.standard.set("12345", forKey: "Password")
        UserDefaults.standard.set("A012345@escuela.mx", forKey: "Correo")
        UserDefaults.standard.set("Liceo del Valle", forKey: "EscuelaUsuario")
        verifyTermsAgreed()
        super.viewDidLoad()
        
        do {
            guard let filePath = Bundle.main.path(forResource: "TerminosyCondicionesESCHOOL", ofType: "html")
                else {
                    // File Error
                    print ("File reading error")
                    return
            }
            let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            webView.loadHTMLString(contents as String, baseURL: baseUrl)
        }
        catch {
            print ("File HTML error")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func verifyTermsAgreed() {
        let userDefs = UserDefaults.standard

        if(userDefs.value(forKey: "termsAccepted") != nil ) {
            performSegue(withIdentifier: "loginSegue", sender: self)
        }
    }
    
    @IBAction func acceptTermsPressed(_ sender: Any) {
        
        UserDefaults.standard.set(true, forKey: "termsAccepted")
    }
}
